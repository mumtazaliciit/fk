import MenuCard from '../components/menu-card'
import Navbar from '../components/navbar'
import './menu.css'


const imageUrl='https://amclaincorporation.files.wordpress.com/2019/07/fast-food-business-card-a.jpg';
const Menu = () => {
    return (
        <div className='menu-main'>
            <Navbar/>
            <div className='navbar-space'></div>
            <h1>checkout the menu</h1>
            <div className='menu-container'>
                <MenuCard imageUrl={imageUrl} />
                <MenuCard imageUrl={imageUrl} />
                <MenuCard imageUrl={imageUrl} />
                <MenuCard imageUrl={imageUrl} />
                <MenuCard imageUrl={imageUrl} />
            </div>
        </div>
    )
}

export default Menu