import { Link } from "react-router-dom"
import Navbar from "../components/navbar";
import './restaurants.css'

const restaurants= [{
    name: 'Fabiha Kitchen F-10 Markaz Islamabad',
    url: '/menu'

},{
    name: 'Fabiha Kitchen F-6 Markaz Islamabad',
    url: '/menu'
}] 

const Restaurants = () => {
    const restaurantsList = restaurants.map((restaurant)=>{
        return (
                <div className='restaurant'>
                    <h2>{restaurant.name}</h2>
                    <Link className='nrm-btn' to={restaurant.url}>Check Menu</Link>
                </div>
        )
    });
    return(
        <div>
            <Navbar/>
            <div className='navbar-space'></div>
            <div className='container'>{restaurantsList}</div>
        </div>        
    );
}

export default Restaurants