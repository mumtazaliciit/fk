import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import Navbar from '../components/navbar';
import Footer from '../components/footer'
import './deals.css'

const dealImg='https://i.pinimg.com/originals/b5/f8/bd/b5f8bdeaf8b7a56dfab2e00bee746db9.jpg';
const dealImg1='https://magazine.foodpanda.pk/wp-content/uploads/sites/13/2016/11/food-12000x444-image2.png';
const dealLink='/';
const title='sample deal for showcase';
const dealContent={
    title,
    dealImg,
};

const dealContent1={
    title,
    dealImg: dealImg1
};

const deals=[{dealImg, dealLink, dealContent},{dealImg: dealImg1, dealLink, dealContent: dealContent1},{dealImg, dealLink, dealContent},{dealImg: dealImg1, dealLink, dealContent: dealContent1},{dealImg, dealLink, dealContent},{dealImg, dealLink, dealContent}
,{dealImg, dealLink, dealContent},{dealImg, dealLink, dealContent},{dealImg, dealLink, dealContent},{dealImg: dealImg1, dealLink, dealContent: dealContent1},{dealImg, dealLink, dealContent}]

class Deals extends Component {
    constructor(props){
        super(props);
        this.state= {
            deals,
            dealClick: 0          
        }
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick=(ind)=>{
        this.setState({dealClick: ind})
    }

    render (){
        const dealsList = this.state.deals.map((deal,ind)=>{
            return (
                <a href='#' className='deal-link' onClick={this.handleClick.bind(this,ind)}>
                    <div className='deal-item'>
                        <img src={deal.dealImg} alt='deal image'/>
                        <h2>Sample Deal Item</h2>
                        <div className='deal-highligter'></div>
                    </div>
                </a>
            );
        });
        return(
            <div>
                <Navbar/>
                <div className='navbar-space'></div>
                <div className='scroll-container'>
                    <div className='scroll-head'><span>DEALS</span></div>
                    <div className='scroll-head-space'></div>
                    <div className='deal-scroll'>
                    {dealsList}
                    </div>
                </div>
                <div className='deal-details'>
                    <img src={deals[this.state.dealClick].dealContent.dealImg} alt='deal image'/>
                    <h2>{deals[this.state.dealClick].dealContent.title}</h2>
                    <Link to='/menu' className='nrm-btn'>Order</Link>
                </div>
                <Footer/>
            </div>
        )
    }
}

export default Deals