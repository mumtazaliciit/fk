import React from 'react'
import Navbar from '../components/navbar'
import HeroImage from '../components/hero-image'
import HomeCard from '../components/home-card'
import './homepage.css'
import DealCard from '../components/deal-card'
import Appboard from '../components/bottom-appboard'
import Footer from '../components/footer'

const cardImageUrl='url(https://res.cloudinary.com/dshitqpca/image/upload/v1608646910/banners/deal-image1_tynefx.png)';
const itemImage='https://res.cloudinary.com/dshitqpca/image/upload/v1608647397/banners/item-image_rht5qp.png';
const itemImage1='https://res.cloudinary.com/dshitqpca/image/upload/v1608722427/images/2-sm-burger-deal_vbewle.png';
const itemImage2='https://res.cloudinary.com/dshitqpca/image/upload/v1608731638/images/item-image2_s6i1gm.png';
const itemImage3='https://res.cloudinary.com/dshitqpca/image/upload/v1608733490/images/item-image3_ntuu6k.png';
const itemImage4='https://res.cloudinary.com/dshitqpca/image/upload/v1608734077/images/item-image4_p4evpn.png';
const dealImageurl='https://res.cloudinary.com/dshitqpca/image/upload/v1608554558/banners/pizza-sm_gmcm8u.png'; 
const dealImageurl2='https://res.cloudinary.com/dshitqpca/image/upload/v1608734378/images/burger1_zmol6p.png';
const dealImageurl3='https://res.cloudinary.com/dshitqpca/image/upload/v1608740128/images/burgers_and_fries-sm_jpaxsb.png';
const marginTopCard='15px 0px 0px 0px';
const marginLeftCard='15px 0.75% 0px 0px';
const marginRightCard='15px 0px 0px 0.75%';

function Homepage() {
  return (
    <div className='homepage'>
      <Navbar/>
      <div className='navbar-space'></div>
      <HeroImage/>
      <div className='container'>
        <HomeCard url={cardImageUrl} itemImage={itemImage} height='300px' width='100%'margin = {marginTopCard} />
        <HomeCard url='rgb(252,68,69)' itemImage={itemImage1} height='300px' width='49.25%' margin={marginLeftCard}/>
        <HomeCard url='rgb(20,167,108)' itemImage={itemImage2} height='300px' width='49.25%'margin={marginRightCard}/>
        <HomeCard url='rgb(255,102,47)' itemImage={itemImage3} height='300px' width='49.25%' margin={marginLeftCard}/>
        <HomeCard url='rgb(55,110,111)' itemImage={itemImage4} height='300px' width='49.25%'margin={marginRightCard}/>
      </div>
      <h1 className='deals-h1'>OUR DEALS</h1>
      <div className='container-sm'>
        <DealCard imageUrl={dealImageurl}/>
        <DealCard imageUrl={dealImageurl2}/>
        <DealCard imageUrl={dealImageurl3}/>
        <DealCard imageUrl={dealImageurl2}/>
        <DealCard imageUrl={dealImageurl3}/>
        <DealCard imageUrl={dealImageurl2}/>
      </div>
      <Appboard/>
      <Footer/>
    </div>
  );
}

export default Homepage