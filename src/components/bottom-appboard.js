import React from 'react'
import './bottom-appboard.css'

const Appboard = () =>{
    return (
            <div className='appboard'>
                <div className='container'>
                    <div className='mobile-app-img'></div>
                    <div className='appboard-textarea'>
                        <h1>install out app for more exiting rewards</h1>
                        <div className='google-apple-icon'>
                            <a href='#google-play'><img src="https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png" alt='google icon'/></a>
                            <a href='#apple-store'><img src="https://www.familybankonline.com/wp-content/uploads/2017/07/apple-app-store-badge.png" alt='google icon'/></a>
                            <p>Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc. Google Play is a trademark of Google Inc. Terms and conditions apply</p>
                        </div>
                    </div>
                </div>
            </div>
    )
}

export default Appboard