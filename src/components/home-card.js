import React from 'react'
import './home-card.css'

const HomeCard = (props)=>{
    const style = {
        background: props.url,
        height: props.height,
        width: props.width,
        display: 'inline-block',
        borderRadius: '10px',
        backgroundSize: '100%' + ' ' + props.height,
        position: 'relative',
        margin: props.margin
    }
    return (
        <a className='home-card' href="#card" style={style}>
            <img className='home-card-img' src={props.itemImage}/>
            <a href='#link' className='card-btn nrm-btn'>button</a>
        </a>
    )
}

export default HomeCard