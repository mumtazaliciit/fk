import './menu-card.css'

const MenuCard = (props)=>{
    return (
        <div class='menu-card'>
            <div className='menu-card-img' style={{backgroundImage: `url(${props.imageUrl})`}}></div>
            <h2 className='menu-card-title'>title is here to be put</h2>
        </div>
    )
}

export default MenuCard