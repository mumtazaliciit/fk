import React, {Component} from 'react'
import './hero-image.css'

class HeroImage extends Component{
   render(){
       return (
           <div className='hero'>
               <h1 className='hero-h1'>a combination of taste and tradition</h1>
               <a href='#link' className='hero-btn nrm-btn'>button</a>
               <a href='#link' className='hero-btn nrm-btn'>button</a>
           </div>
       )
   }
}

export default HeroImage;