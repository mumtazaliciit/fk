import React from 'react'
import './navbar.css'
import {Link} from 'react-router-dom'

const Navbar = ()=>(
    <div className='navbar'>
        <div class="topnav">
            <Link className='logo' to='/'><img src="image/logo.png" alt='logo'/></Link>
            <div className="menu-btn nav-btn">
                <a className="drop-btn" href="#restaurants"><i class="fas fa-bars"></i></a>
                <div class="dropdown-content">
                    <a href="#">about us</a>
                    <a href="#">join FK</a>
                    <hr class="solid-hr"></hr>
                    <a href="#">terms of service</a>
                    <a href="#">privacy policy</a>
                    <a href="#">accesibility</a>
                    <hr class="solid-hr"></hr>
                    <a href="#">contact us</a>
                    <a href="#">FAQs</a>
                </div>
            </div> 
            <a className="nav-btn" href="/restaurants">Restaurants</a>
            <Link className="nav-btn" to='/menu'>Order</Link>
            <a className="nav-btn" href="/deals">Deals</a>
            <a className='nav-cart' href='#cart'><i class="fas fa-shopping-basket"></i></a>
            <a className="nrm-btn" href="#about">sign in</a>
            <a className="nrm-btn" href="#about">sign up</a>
        </div>
        <div className='nav-msg'></div>
    </div>
)

export default Navbar