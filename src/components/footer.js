import React from 'react'
import './footer.css'

const Footer = ()=>{
    return(
		<footer class="footer-distributed">
		    <div class="footer-left">
                <h3>Fabiha's<span>Kitchen</span></h3>
                <p class="footer-links">
                    <a href="#">Home</a>
                    <a href="#">About</a>
                    <a href="#">Faq</a>
                    <a href="#">Contact</a>
                </p>
                <p class="footer-company-name">Fabiha's Kitchen &copy; 2021</p>
		    </div>
 
		    <div class="footer-center">
                <div>
		            <i class="fa fa-map-marker"></i>
		            <p><span>21 Revolution Street</span> Lahore, Pakistan</p>
		        </div>
                <div>
                    <i class="fa fa-phone"></i>
                    <p>+x xxx xxxxxxx</p>
                </div>
                <div>
		            <i class="fa fa-envelope"></i>
		                <p><a href="mailto:support@company.com">contact@fk.com</a></p>
		        </div>
 		    </div>
 
		    <div class="footer-right">
                <p class="footer-company-about">
                    <span>About the company</span>
                    Fabiha Kitchen is sample app created in ReactJS to showcase skills involved in food website.
                </p>
 		        <div class="footer-icons">
                     <a href="#"><i class="fab fa-facebook-square"></i></a>
                     <a href="#"><i class="fab fa-twitter-square"></i></a>
                     <a href="#"><i class="fab fa-linkedin"></i></a>
                </div>
		    </div>
 		</footer>
    )
}

export default Footer