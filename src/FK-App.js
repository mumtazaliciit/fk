import { Route, Switch } from 'react-router-dom'
import Deals from './containers/deals'
import Homepage from './containers/homepage'
import Menu from './containers/menu'
import Restaurants from './containers/restaurants'

const Routing = ()=>{
    return(
        <Switch>
            <Route path='/' component={Homepage} exact />
            <Route path='/menu' component={Menu} />
            <Route path='/restaurants' component={Restaurants} />
            <Route path='/deals' component={Deals} />
            <Route component={()=>{
                return (
                    <div>
                        something went wrong!
                    </div>
                )
            }} />
        </Switch>
    )
}

export default Routing;